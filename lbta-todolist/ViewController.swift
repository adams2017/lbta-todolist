//
//  ViewController.swift
//  lbta-todolist
//
//  Created by Admin on 4/10/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

class ViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var  tasks = ["buy eggs", "change ok", "get dry cleaning"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    func setup() {
        
        // register, numberofcells, size, cellforitem, dequeue  methods needed
        collectionView.backgroundColor = .red
        navigationItem.title = "To-Do List"
        collectionView.alwaysBounceVertical = true
        
        collectionView.register(TaskCell.self, forCellWithReuseIdentifier: TaskCell.reuseIdentifier)
        collectionView.register(TaskHeader.self,
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                withReuseIdentifier: TaskHeader.reuseIdentifier)
    }
    // Query event for header: VIEWFORSUPPLEMENTARYELEMENTOFKIND, FOLLOWED BY DEQUEUE
       override func collectionView(_ collectionView: UICollectionView,
                                 viewForSupplementaryElementOfKind kind: String,
                                 at indexPath: IndexPath) -> UICollectionReusableView {
        
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader,
                                                                withReuseIdentifier: TaskHeader.reuseIdentifier,
                                                                for: indexPath) as! TaskHeader
            header.delegate = self
        return header
    }
    // Query event for header: REFERENCESIZEFORHEADERINSECTION
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 50)
    }
    //Query event for number of items in section
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tasks.count
    }
    //Query event for cel
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TaskCell.reuseIdentifier, for: indexPath) as! TaskCell
        cell.nameLabel.text = tasks[indexPath.item]
        return cell
    }
    //Query event for size of cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 50)
    }
    
    func addTask(_ nameOfTask: String) {
        tasks.append(nameOfTask)        
        collectionView.reloadData()        
    }
}



