//
//  AppDelegate.swift
//  lbta-todolist
//
//  Created by Admin on 4/10/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow()
        window?.makeKeyAndVisible()
        window?.rootViewController = UINavigationController(
            rootViewController: ViewController(collectionViewLayout: UICollectionViewFlowLayout()))
        return true
    }

}

