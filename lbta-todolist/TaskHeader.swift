//
//  TaskHeader.swift
//  lbta-todolist
//
//  Created by Admin on 4/10/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

class TaskHeader: UICollectionViewCell {
    //Mark: PROPERTIES
    static var reuseIdentifier: String {
        return self.description()
    }
    var delegate:ViewController?
    
    //MARK: LIFECYCLE
    override init(frame: CGRect) {
        super .init(frame: frame)
        setup()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    func setup() {
        [taskTextField, addButton].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview($0)
        }
        // |-8-text field -8- button-8-|
        NSLayoutConstraint.activate([
            taskTextField.topAnchor.constraint(equalTo: self.topAnchor, constant: 8),
            taskTextField.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),
            taskTextField.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8),
            taskTextField.widthAnchor.constraint(equalToConstant: 250),
            
            addButton.topAnchor.constraint(equalTo: taskTextField.topAnchor, constant: 0),
            addButton.leftAnchor.constraint(equalTo: taskTextField.rightAnchor, constant: 8),
            addButton.bottomAnchor.constraint(equalTo: taskTextField.bottomAnchor, constant: 0),
            addButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0)
            ])
    }
    //MARK: UI Elements =======================================================
    let taskTextField:UITextField  = {
        let ui = UITextField()
        ui.placeholder = "Your task"
        ui.backgroundColor = UIColor.cyan
        ui.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: 0))
        ui.leftViewMode = UITextField.ViewMode.always
        ui.layer.borderColor = UIColor.lightGray.cgColor
        ui.layer.borderWidth = 1
        ui.layer.cornerRadius = 16
        return ui
    }()
    
    lazy var addButton:UIButton = {  //snip zxuibtnl
        let ui = UIButton(type: .system)
        ui.setTitle("Add", for: .normal)
        ui.titleLabel?.font = .systemFont(ofSize: 20)
        ui.setTitleColor(UIColor.white, for: UIControl.State.normal)
        ui.backgroundColor = UIColor.orange
        ui.layer.borderWidth = 1
        ui.layer.cornerRadius = 16
        ui.addTarget(self, action: #selector(addButtonTouchSelector), for: .touchUpInside)
        return ui
    }()
    
    @objc fileprivate func addButtonTouchSelector(sender:Any) {
        print("add button touch selector ")
        delegate?.addTask(taskTextField.text ?? "") //add task is in the "ViewController" class,
        //not his class. The tasks array is defined there, as well as the colectionview which must be reloaded
        taskTextField.text = nil
    }
}

