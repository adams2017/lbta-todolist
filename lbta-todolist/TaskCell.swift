import UIKit

class TaskCell: UICollectionViewCell {
    //Mark: PROPERTIES
    static var reuseIdentifier: String {
        return self.description()
    }
    //MARK: LIFECYCLE
    override init(frame: CGRect) {
        super .init(frame: frame)
        setup()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var task:TaskModel? {
        didSet {
            guard let task = task else {return}
        }
    }
    
    func setup() {
    
        _=[nameLabel ].map {
            $0.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview($0)
        }
        let constraints=[
            nameLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            nameLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),
            nameLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            nameLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8)
        ]
        _=constraints.map { $0.isActive=true }
     
        print(constraints.count)
        print(type(of: constraints[0]))
        print(constraints[0])
        
    }
    //MARK: UI Elements =======================================================
    let nameLabel:UILabel = {
        let ui = UILabel()
        ui.text = "Sample Label"
        ui.backgroundColor = .brown
        return ui
    }()
}

